//// Convert a TH2D of VLQ mass limit vs. BR into HEP data format (YAML)
//// J. Haley <joseph.haley@cern.ch>
//// 2017 April 14

#include "TFile.h"
#include "TH2D.h"
#include "TH2F.h"
#include <string>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include "TGraph2D.h"
#include "TGraph.h"
#include <map>


using namespace std;

string lumi = "36.1"; // integrated luminosity
string cme = "13000"; // center of mass energy [GeV]


map<string, float> signalSamples;
// cross-sections in [pb]

bool Majorana;

void initialize_map_elements(){
  signalSamples["2100_1700"] = 4.335e-05*1.0e+03;
  signalSamples["2400_200"]  = 1.076e-04*1.0e+03;
  signalSamples["2400_400"]  = 9.782e-05*1.0e+03;
  signalSamples["3000_2100"] = 1.046e-05*1.0e+03;
  signalSamples["3000_2500"] = 4.147e-06*1.0e+03;
  signalSamples["3500_200"] =1.028e-05*1.0e+03;
  signalSamples["3500_300"] =1.008e-05*1.0e+03;
  signalSamples["600_50"] = 51.02;
  signalSamples["600_150"]= 49.713;
  signalSamples["600_300"]= 37.33;
  signalSamples["600_450"]= 16.138;
  signalSamples["600_500"]= 8.7848;
  signalSamples["600_600"]= 0.36365;
  signalSamples["600_700"]= 0.079408;
  signalSamples["600_900"]= 0.015643;
  signalSamples["600_1200"]=  0.0031095;
  signalSamples["1000_700"]=  2.7778;
  signalSamples["1000_800"]=  1.5465;
  signalSamples["1000_1000"]= 0.044133;
  signalSamples["1000_1100"]= 0.015058;
  signalSamples["1000_1400"]= 2.0290e-06*1.0e+03;
  signalSamples["1200_50"]= 3.2448;
  signalSamples["1200_300"]=3.1065;
  signalSamples["1200_600"]=2.3153;
  signalSamples["1200_900"]=0.98898;
  signalSamples["1200_1100"]=0.17725;
  signalSamples["1200_1200"]=0.019283;
  signalSamples["1200_1500"]=0.001777;
  signalSamples["1200_1800"]=0.0004383;
  signalSamples["1200_2400"]=5.162e-05;
  signalSamples["1500_1000"]=0.5361;
  signalSamples["1500_1100"]=0.39588;
  signalSamples["1500_1200"]=0.25818;
  signalSamples["1500_1600"]=3.2349e-06*1.0e+03;
  signalSamples["1500_1800"]=7.2710e-07*1.0e+03;
  signalSamples["1800_50"]= 0.50828;
  signalSamples["1800_450"]= 0.47505;
  signalSamples["1800_900"]= 0.3507;
  signalSamples["1800_1350"]= 0.14888;
  signalSamples["1800_1500"]= 0.079323;
  signalSamples["1800_1600"]= 0.04133;
  signalSamples["1800_1700"]= 0.01393;
  signalSamples["1800_2700"]= 2.9008e-05;
  signalSamples["1800_3600"]= 1.9213e-06;
  signalSamples["2100_1500"]= 8.1807e-05*1.0e+03;
  signalSamples["2100_1800"]= 2.7776e-05*1.0e+03;
  signalSamples["2100_2200"]= 7.2120e-07*1.0e+03;
  signalSamples["2400_50"]= 0.11263;
  signalSamples["2400_600"]= 0.10148;
  signalSamples["2400_1200"]= 0.074743;
  signalSamples["2400_1800"]= 0.031435;
  signalSamples["2400_1900"]= 0.023783;
  signalSamples["2400_2100"]= 0.010368;
  signalSamples["2400_2300"]= 0.001897;
  signalSamples["2400_3600"]= 2.5613e-06;
  signalSamples["2400_4800"]= 9.7783e-08;
  signalSamples["2700_300"]= 5.4544e-05*1.0e+03;
  signalSamples["3000_30"]= 0.032707;
  signalSamples["3000_50"]= 0.030543;
  signalSamples["3000_150"]= 0.029708;
  signalSamples["3000_300"]= 0.02845;
  signalSamples["3000_750"]= 0.026238;
  signalSamples["3000_1500"]= 0.018827;
  signalSamples["3000_1600"]= 0.017545;
  signalSamples["3000_1800"]= 0.014728;
  signalSamples["3000_2250"]= 0.0078485;
  signalSamples["3000_2900"]= 0.00033845;
  signalSamples["3000_4500"]= 2.5493e-07;
  signalSamples["3000_6000"]= 7.3718e-09;
  signalSamples["3500_50"]= 0.011405;
  signalSamples["3500_400"]= 9.9448e-06*1.0e+03;
  signalSamples["3500_875"]= 0.0091305;
  signalSamples["3500_1750"]= 0.0064078;
  signalSamples["3500_2625"]= 0.002618;
  signalSamples["3500_3400"]= 9.027e-05;
  signalSamples["3500_5250"]= 3.984e-08;
  signalSamples["3500_7000"]= 1.3538e-09;
  signalSamples["3600_50"]= 0.0094965;
  signalSamples["3600_900"]= 0.0073963;
  signalSamples["3600_1800"]= 0.005172;
  signalSamples["3600_2700"]= 0.0021193;
  signalSamples["3600_3500"]= 7.013e-05;
  signalSamples["3600_5400"]= 2.7605e-08;
  signalSamples["3600_7200"]= 1.0035e-09;
  signalSamples["3800_3000"]= 1.0520e-06*1.0e+03;
  signalSamples["4000_400"]= 0.0038988;
  signalSamples["4200_50"]= 0.0033228;
  signalSamples["4200_200"]= 3.0004e-06*1.0e+03;
  signalSamples["4200_400"]= 2.6964e-06*1.0e+03;
  signalSamples["4200_1050"]= 0.0022143;
  signalSamples["4200_2100"]= 0.0014893;
  signalSamples["4200_3150"]= 0.00059705;
  signalSamples["4200_4100"]= 1.557e-05;
  signalSamples["4200_6300"]= 3.4015e-09;
  signalSamples["4200_8400"]= 2.1228e-10;
  signalSamples["4500_50"]= 0.0020453;
  signalSamples["4500_800"]= 1.420e-06*1.0e+03;
  signalSamples["4500_1125"]=0.0012275;
  signalSamples["4500_2250"]=0.00080206;
  signalSamples["4500_3375"]=0.00031672;
  signalSamples["4500_4400"]=7.4606e-06;
  signalSamples["4750_3000"]= 3.3310e-07*1.0e+03;
  signalSamples["5000_50"]=0.0010374;
  signalSamples["5000_500"]=0.00068688;
  signalSamples["5000_1250"]=0.00047349;
  signalSamples["5000_2500"]=0.00028817;
  signalSamples["5000_3750"]=0.00011102;
  signalSamples["5000_4900"]=2.2329e-06;
  signalSamples["5000_4900"]=2.2329e-06;
  signalSamples["5200_1000"]=3.8360e-07*1.0e+03;
  signalSamples["5200_2000"]=2.5260e-07*1.0e+03;
  signalSamples["5200_3000"]=1.5610e-07*1.0e+03;
  signalSamples["5800_500"]=2.2190e-07*1.0e+03;
  signalSamples["5800_1000"]= 1.5420e-07*1.0e+03;
  signalSamples["5800_2000"]=8.5500e-08*1.0e+03;
  signalSamples["5800_3000"]=5.3700e-08*1.0e+03;
  
}

void dump_observed(std::vector<vector<double>> x, std::vector<vector<double>> y, std::vector<vector<double>> UL, string limit_type, std::ofstream &os, double mu_sig){

  os << "- header: {name: \'Cross section upper limit at 95% CL\', units: pb}" << endl;
  os << "  qualifiers:" << endl;
  os << "  - {name: Limit, value: " << limit_type << "}" << endl;
  os << "  - {name: SQRT(S), units: GeV, value: " << cme << "}" << endl;
  os << "  - {name: LUMINOSITY, units: 'fb$^{-1}$', value: " << lumi << "}" << endl;
  os << "  values:" << endl;

  for(u_int i = 0; i < UL[0].size(); ++i){
    string cross_section = std::to_string((int)x[0][i])+"_"+std::to_string((int)y[0][i]);
    if(UL[0][i]>1e-6) os << "  - {value: "  << UL[0][i]*1/3*signalSamples[cross_section]*mu_sig << "}" << endl;  // the unphysical regions bin content set to 0
      else      os << "  - {value: \'-\'}" << endl;             // so only consider bins > 0
  }
}

void dump_contour(std::vector<vector<double>> y, string limit_type, std::ofstream &os, int which){

  os << "- header: {name: Mass WR, units: TeV}" << endl;
  os << "  qualifiers:" << endl;
  os << "  - {name: Limit, value: " << limit_type << "}" << endl;
  os << "  - {name: SQRT(S), units: GeV, value: " << cme << "}" << endl;
  os << "  - {name: LUMINOSITY, units: 'fb$^{-1}$', value: " << lumi << "}" << endl;
  os << "  values:" << endl;

  for(u_int i = 0; i < y[which].size(); ++i){
    if(y[which][i]>1e-6) os << "  - {value: "  << y[which][i]/1000. << "}" << endl;  // the unphysical regions bin content set to 0
    else      os << "  - {value: \'-\'}" << endl;             // so only consider bins > 0
  }
}

void dump_observed_mu(std::vector<vector<double>> x, std::vector<vector<double>> y, std::vector<vector<double>> UL, string limit_type, std::ofstream &os){

  os << "- header: {name: \'Excluded at 95% CL\'}" << endl;
  os << "  qualifiers:" << endl;
  os << "  - {name: Limit, value: " << limit_type << "}" << endl;
  os << "  - {name: SQRT(S), units: GeV, value: " << cme << "}" << endl;
  os << "  - {name: LUMINOSITY, units: 'fb$^{-1}$', value: " << lumi << "}" << endl;
  os << "  values:" << endl;

  for(u_int i = 0; i < UL[0].size(); ++i){
    string cross_section = std::to_string((int)x[0][i])+"_"+std::to_string((int)y[0][i]);
    //if(UL[0][i]>1e-6) os << "  - {value: "  << UL[0][i] << "}" << endl;  // the unphysical regions bin content set to 0
    // else      os << "  - {value: \'-\'}" << endl;             // so only consider bins > 0
    if(UL[0][i]<1) os << "  - {value: "  << 1 << "}" << endl;  // the unphysical regions bin content set to 0
    else if(UL[0][i]>1)      os << "  - {value: "  << 0 << "}" << endl;  // the unphysical regions bin content set to 0
  }
}

void dump_efficiency(std::vector<double> eff, string efficiency, std::ofstream &os){

  os << "- header: {name: \'Efficiency times acceptance\', units: \'%\'}" << endl;
  os << "  qualifiers:" << endl;
  os << "  - {name: Efficiency times acceptance, value: " << efficiency << "}" << endl;
  os << "  - {name: SQRT(S), units: GeV, value: " << cme << "}" << endl;
  os << "  - {name: LUMINOSITY, units: 'fb$^{-1}$', value: " << lumi << "}" << endl;
  os << "  values:" << endl;

  for(u_int i = 0; i < eff.size(); ++i)
    os << "  - {value: " << eff[i] << "}" << endl;

}

void dump_expected_pm_1and2sigma(std::vector<vector<double>>x, std::vector<vector<double>>y,std::vector<vector<double>> UL, string limit_type, std::ofstream &os, double mu_sig){

  // + error band
  os << "- header: {name: \'Cross section upper limit at 95% CL\', units: pb}" << endl;
  os << "  qualifiers:" << endl;
  os << "  - {name: Limit, value: Expected}" << endl;
  os << "  - {name: SQRT(S), units: GeV, value: " << cme << "}" << endl;
  os << "  - {name: LUMINOSITY, units: 'fb$^{-1}$', value: " << lumi << "}" << endl;
  os << "  values:" << endl;

  for(int i = 0; i < UL[1].size(); ++i){
    string cross_section = std::to_string((int)x[1][i])+"_"+std::to_string((int)y[1][i]);
    os << "  - value: " << UL[1][i]*1/3*mu_sig*signalSamples[cross_section] << endl;
    os << "    errors: " << endl;
    os << "    - {asymerror: {plus: "  << UL[2][i]*1/3*mu_sig*signalSamples[cross_section] << 
      ", minus: " << -UL[4][i]*1/3*mu_sig*signalSamples[cross_section] << "}, label: '1 sigma'}" << endl;
    os << "    - {asymerror: {plus: "  << UL[3][i]*1/3*mu_sig*signalSamples[cross_section] << 
      ", minus: " << -UL[5][i]*1/3*mu_sig*signalSamples[cross_section] << "}, label: '2 sigma'}" << endl;
  }
}

void dump_expected_pm_1and2sigma_mu(std::vector<vector<double>>x, std::vector<vector<double>>y,std::vector<vector<double>> UL, string limit_type, std::ofstream &os){

  // + error band
  os << "- header: {name: \'Excluded at 95% CL\'}" << endl;
  os << "  qualifiers:" << endl;
  os << "  - {name: Limit, value: Expected}" << endl;
  os << "  - {name: SQRT(S), units: GeV, value: " << cme << "}" << endl;
  os << "  - {name: LUMINOSITY, units: 'fb$^{-1}$', value: " << lumi << "}" << endl;
  os << "  values:" << endl;

  for(int i = 0; i < UL[1].size(); ++i){
    string cross_section = std::to_string((int)x[1][i])+"_"+std::to_string((int)y[1][i]);
    if(UL[1][i]<1) os << "  - {value: "  << 1 << "}" << endl;  // the unphysical regions bin content set to 0
    else if(UL[1][i]>1)      os << "  - {value: "  << 0 << "}" << endl;  // the unphysical regions bin content set to 0
    //os << "  - value: " << UL[1][i] << endl;
    //os << "    errors: " << endl;
    //os << "    - {asymerror: {plus: "  << UL[2][i] <<  ", minus: " << -UL[4][i] << "}, label: '1 sigma'}" << endl;
    //os << "    - {asymerror: {plus: "  << UL[3][i] <<  ", minus: " << -UL[5][i] << "}, label: '2 sigma'}" << endl;
  }
}

void Efficiency(string fn1){

  TFile * _file1 = new TFile(fn1.c_str(),"OPEN");

  if(_file1==NULL){ cout << "Attention: the file you gave me is NULL. Please check. Now abort." << endl; return;}


  TGraph2D* h_SS_ee = 0;   TGraph2D* h_SS_mm = 0;
  TGraph2D* h_OS_ee = 0;   TGraph2D* h_OS_mm = 0;

  h_SS_ee    = (TGraph2D*)_file1->Get("SSee");
  h_SS_mm    = (TGraph2D*)_file1->Get("SSmm");
  h_OS_ee    = (TGraph2D*)_file1->Get("OSee");
  h_OS_mm    = (TGraph2D*)_file1->Get("OSmm");


  std::vector<TGraph2D*> Graphs;
  
 // Please remember filling order MATTERS.
  Graphs.push_back(h_SS_ee); Graphs.push_back(h_SS_mm);  
  Graphs.push_back(h_OS_ee); Graphs.push_back(h_OS_mm);  

  assert(h_SS_ee); assert(h_SS_mm); assert(h_OS_ee); assert(h_OS_mm);

  int bins = Graphs[0]->GetN();

  std::vector<vector<double>> v_x;
  std::vector<vector<double>> v_y;
  std::vector<vector<double>> v_z;


  for(auto histo=0;histo<4;histo++){
      auto x = Graphs[histo]->GetX();
      auto y = Graphs[histo]->GetY();
      auto z = Graphs[histo]->GetZ();
      std::vector<double> tmpx;
      std::vector<double> tmpy;
      std::vector<double> tmpz;
      for(auto n=0; n<bins; n++){
        tmpx.push_back(x[n]);
        tmpy.push_back(y[n]);
        tmpz.push_back(z[n]);
      }
      v_x.push_back(tmpx);
      v_y.push_back(tmpy);
      v_z.push_back(tmpz);
      tmpx.clear();
      tmpy.clear();
      tmpz.clear();
  }

  // Now I have to define 4 outfiles one per graph
  string outfileName[4]={"data_SS_ee.yaml","data_SS_mm.yaml","data_OS_ee.yaml","data_OS_mm.yaml"};


  for (auto j =0; j< 4; j++){
    std::ofstream os(outfileName[j], std::ofstream::out);
    os << "independent_variables:" << endl;

    os << "- header: {name: Mass WR, units: TeV}" << endl;
    os << "  values:" << endl;
    
    for(int i = 0; i < v_x[0].size(); ++i){
        os << "  - {value: " << float(v_x[j][i]) << "}" << endl;
    }

    os << "- header: {name: Mass NR, units: TeV}" << endl;
    os << "  values:" << endl;
    
    for(int i = 0; i < v_y[0].size(); ++i){
      os << "  - {value: " << float(v_y[j][i]) << "}" << endl;
    }
    
    os << "dependent_variables:" << endl;

    dump_efficiency(v_z[j], "Efficiency", os);
    os.precision(10);
    os.close();
  }

}


void Limit2DContour(std::string fn1, std::string chan, std::string hypo){

  TFile * _file1 = new TFile(fn1.c_str(),"OPEN");

  if(_file1==NULL){ cout << "Attention: the file you gave me is NULL. Please check. Now abort." << endl; return;}

  TGraph* h_observed = 0; TGraph* h_expected = 0;

  h_expected    = (TGraph*)_file1->Get("SubGraphs/expectedUpperLimit_Contour_0");
  h_observed    = (TGraph*)_file1->Get("SubGraphs/upperLimit_Contour_0");

  std::vector<TGraph*> Graphs;
  // Please remember filling order MATTERS.
  Graphs.push_back(h_observed);  Graphs.push_back(h_expected);

  assert(h_observed); assert(h_expected); 
  
  int bins1 = Graphs[0]->GetN();
  int bins2 = Graphs[0]->GetN();

  std::vector<vector<double>> v_x;
  std::vector<vector<double>> v_y;

  for(auto histo=0;histo<2;histo++){
      auto x = Graphs[histo]->GetX();
      auto y = Graphs[histo]->GetY();
      std::vector<double> tmpx;
      std::vector<double> tmpy;
      int bins;
      if(histo==0)bins = bins1;
      else bins=bins2;
      for(auto n=0; n<bins; n++){
        tmpx.push_back(x[n]);
        tmpy.push_back(y[n]);
      }
      v_x.push_back(tmpx);
      v_y.push_back(tmpy);
      tmpx.clear();
      tmpy.clear();
  }

  string expoutfileName_c="expcontour_"+chan+"_"+hypo+".yaml";
  string obsoutfileName_c="obspcontour_"+chan+"_"+hypo+".yaml";

  std::ofstream os_c_exp (expoutfileName_c, std::ofstream::out);
  std::ofstream os_c_obs (obsoutfileName_c, std::ofstream::out);

  os_c_exp << "independent_variables:" << endl;
  os_c_obs << "independent_variables:" << endl;
  
  os_c_exp << "- header: {name: Mass WR, units: TeV}" << endl;
  os_c_exp << "  values:" << endl;

  os_c_obs << "- header: {name: Mass WR, units: TeV}" << endl;
  os_c_obs << "  values:" << endl;

  for(int i = 0; i < v_x[1].size(); ++i){
    os_c_exp << "  - {value: " << float(v_x[1][i])/1000. << "}" << endl;
  }

  for(int i = 0; i < v_x[0].size(); ++i){
    os_c_obs << "  - {value: " << float(v_x[0][i])/1000. << "}" << endl;
  }
  
  os_c_exp << "dependent_variables:" << endl;
  os_c_obs << "dependent_variables:" << endl;
  
  dump_contour(v_y,"Expected",os_c_exp, 1);
  dump_contour(v_y,"Observed",os_c_obs, 0);

  os_c_exp.close();
  os_c_obs.close();
}

void Limit2D(std::string fn1, std::string chan, std::string hypo, double mu_sig){

  initialize_map_elements();

  TFile * _file1 = new TFile(fn1.c_str(),"OPEN");

  if(_file1==NULL){ cout << "Attention: the file you gave me is NULL. Please check. Now abort." << endl; return;}

  TGraph2D* h_observed = 0; TGraph2D* h_expected = 0;
  TGraph2D* h_expectedP1S = 0;   TGraph2D* h_expectedP2S = 0;
  TGraph2D* h_expectedM1S = 0;   TGraph2D* h_expectedM2S = 0;

  h_expected    = (TGraph2D*)_file1->Get("expectedUpperLimit_gr");
  h_expectedP1S    = (TGraph2D*)_file1->Get("expectedUpperLimitPlus1Sig_gr");
  h_expectedP2S    = (TGraph2D*)_file1->Get("expectedUpperLimitPlus2Sig_gr");
  h_expectedM1S    = (TGraph2D*)_file1->Get("expectedUpperLimitMinus1Sig_gr");
  h_expectedM2S    = (TGraph2D*)_file1->Get("expectedUpperLimitMinus2Sig_gr");
  h_observed    = (TGraph2D*)_file1->Get("upperLimit_gr");

  std::vector<TGraph2D*> Graphs;
  // Please remember filling order MATTERS.
  Graphs.push_back(h_observed);  Graphs.push_back(h_expected); Graphs.push_back(h_expectedP1S);
  Graphs.push_back(h_expectedP2S);  Graphs.push_back(h_expectedM1S); Graphs.push_back(h_expectedM2S);

  assert(h_observed); assert(h_expected); assert(h_expectedP1S); assert(h_expectedP2S); assert(h_expectedM1S); assert(h_observedM2S);

  int bins = Graphs[0]->GetN();

  std::vector<vector<double>> v_x;
  std::vector<vector<double>> v_y;
  std::vector<vector<double>> v_z;


  for(auto histo=0;histo<6;histo++){
      auto x = Graphs[histo]->GetX();
      auto y = Graphs[histo]->GetY();
      auto z = Graphs[histo]->GetZ();
      std::vector<double> tmpx;
      std::vector<double> tmpy;
      std::vector<double> tmpz;
      for(auto n=0; n<bins; n++){
        tmpx.push_back(x[n]);
        tmpy.push_back(y[n]);
        tmpz.push_back(z[n]);
      }
      v_x.push_back(tmpx);
      v_y.push_back(tmpy);
      v_z.push_back(tmpz);
      tmpx.clear();
      tmpy.clear();
      tmpz.clear();
  }

  string outfileName_xsec="xsec_"+chan+"_"+hypo+".yaml";
  std::ofstream os_x (outfileName_xsec, std::ofstream::out);

  os_x << "independent_variables:" << endl;
  
  os_x << "- header: {name: Mass WR, units: TeV}" << endl;
  os_x << "  values:" << endl;
  for(int i = 0; i < v_x[0].size(); ++i){
    os_x << "  - {value: " << float(v_x[0][i])/1000. << "}" << endl;
  }
  
  os_x << "- header: {name: Mass NR, units: TeV}" << endl;
  os_x << "  values:" << endl;
  for(int i = 0; i < v_y[0].size(); ++i){
    os_x << "  - {value: " << float(v_y[0][i])/1000. << "}" << endl;
  }
    
  
  os_x << "dependent_variables:" << endl;
  
  dump_observed(v_x,v_y,v_z,"Observed",  os_x, mu_sig);
  dump_expected_pm_1and2sigma(v_x,v_y,v_z,"Expected", os_x, mu_sig);
  os_x.close();

  //Signal strenght
  string outfileName_mu="mu_"+chan+"_"+hypo+".yaml";
  std::ofstream os_mu (outfileName_mu, std::ofstream::out);

  os_mu << "independent_variables:" << endl;
  
  os_mu << "- header: {name: Mass WR, units: TeV}" << endl;
  os_mu << "  values:" << endl;
  for(int i = 0; i < v_x[0].size(); ++i){
    os_mu << "  - {value: " << float(v_x[0][i])/1000. << "}" << endl;
  }
  
  os_mu << "- header: {name: Mass NR, units: TeV}" << endl;
  os_mu << "  values:" << endl;
  for(int i = 0; i < v_y[0].size(); ++i){
    os_mu << "  - {value: " << float(v_y[0][i])/1000. << "}" << endl;
  }
  
  os_mu << "dependent_variables:" << endl;
  
  dump_observed_mu(v_x,v_y,v_z,"Observed",  os_mu);
  dump_expected_pm_1and2sigma_mu(v_x,v_y,v_z,"Expected", os_mu);
  os_mu.close();

}


void Limit2D_to_YAML(){

  cout << " Hello, I am your Heavy Neutrino YAML converter." << endl;

  Limit2DContour("180314_ttbarSysv4_symFull_sepSys_rmSig_mmos_2contours.root","mm","Dirac");
  Limit2DContour("180314_ttbarSysv4_symFull_sepSys_rmSig_eeos_2contours.root","ee","Dirac");
  Limit2DContour("mm_contour_linear_muSig.root","mm","Majorana");
  Limit2DContour("ee_contour_linear_muSig.root","ee","Majorana");

  Limit2D("180314_ttbarSysv4_symFull_sepSys_rmSig_mmos_2contours.root","mm","Dirac",0.5);
  Limit2D("180314_ttbarSysv4_symFull_sepSys_rmSig_eeos_2contours.root","ee","Dirac",0.5);
  Limit2D("mm_contour_linear_muSig.root","mm","Majorana",1);
  Limit2D("ee_contour_linear_muSig.root","ee","Majorana",1);

  Efficiency("efficiencies_interp.root");
  
  return;

}

